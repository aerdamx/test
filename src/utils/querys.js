export const topicLevelQuery = `
  relatedTopics {
    name
    id
    stargazerCount
  }
`

export const topicRootQuery = (topicLevel, name) => `
  query {
    topic(name: "${name}") {
    name
    id
    stargazerCount
      ${topicLevel}
    }
  }
`

export const topicQueryMaker = (levels = 1, name = 'react') => {
  let topicLevels = ''
  for (let i = 0; i < levels; i++) {
    if (i === 0) {
      topicLevels = topicLevelQuery
    } else {
      topicLevels = topicLevels.replace('}', `${topicLevelQuery}}`)
    }
  }

  return topicRootQuery(topicLevels, name)
}
