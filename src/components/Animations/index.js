import styled from 'styled-components'
import Lottie from 'react-lottie'

// Lottie files
import lottieLoader from './../../lottie/loader.json'
import lottieSad from './../../lottie/sad.json'

// utils
import { lottieDefaults } from '../../utils/configs'

const LoadingDiv = styled.div`
  text-align: center;
  font-size: 3em;
  position: absolute;
  z-index: 2;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -300%);
`

const Wrapper = styled.div`
  position: relative;
`

const Animation = ({ type, msg }) => {
  const animationData = type === 'loader' ? lottieLoader : lottieSad

  return (
    <Wrapper>
      <LoadingDiv>{msg}</LoadingDiv>
      <Lottie
        options={{ ...lottieDefaults, animationData }}
        height={600}
        width={600}
      />
    </Wrapper>
  )
}

export default Animation
