import TopicItem from './../TopicItem'
import { OL } from './../../styled/index'

const TopicRecursive = ({ level, relatedTopics, handleSetLevel, ...topic }) => {
  let hasChildren = true
  const newLevel = level + 1
  if (!Array.isArray(relatedTopics)) {
    hasChildren = !hasChildren
  }

  let View
  if (hasChildren) {
    View = relatedTopics.map((topic) => (
      <OL number={newLevel} key={topic.id}>
        {topic.name} : {topic.stargazerCount}
        <TopicRecursive
          handleSetLevel={handleSetLevel}
          {...topic}
          level={newLevel}
        />
      </OL>
    ))
  } else {
    View = (
      <TopicItem
        key={topic.id}
        handleSetLevel={handleSetLevel}
        {...topic}
        level={newLevel}
      />
    )
  }

  return View
}

export default TopicRecursive
