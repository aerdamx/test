import { LI } from './../../styled/index'

const TopidItem = ({ id, name, stargazerCount, level, handleSetLevel }) => {
  return (
    <LI number={level} key={id} onClick={() => handleSetLevel(level + 1)}>
      {name} : {stargazerCount}
    </LI>
  )
}

export default TopidItem
