import App from './App'
import TestRenderer from 'react-test-renderer'
import { act } from 'react-dom/test-utils'
import { MockedProvider } from '@apollo/client/testing'
import { gql } from '@apollo/client'

// Utils
import { topicQueryMaker } from './utils/querys'

const mocks = [
  {
    request: {
      query: gql`
        ${topicQueryMaker(1)}
      `,
    },
    result: {
      data: {
        topic: {
          id: 'MDU6VG9waWNyZWFjdA==',
          name: 'react',
          relatedTopics: [],
        },
      },
    },
    error: new Error('Something went wrong'),
  },
]

it('Should render the loading', () => {
  const component = TestRenderer.create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <App />
    </MockedProvider>,
  )

  const tree = component.toJSON()
  expect(tree.children[0].children[0]).toContain('Loading....')
})

it('should render first list', async () => {
  const dogMock = {
    request: {
      query: gql`
        ${topicQueryMaker(1)}
      `,
    },
    result: {
      data: {
        topic: {
          id: 'MDU6VG9waWNyZWFjdA==',
          name: 'react',
          stargazerCount: 23456,
          relatedTopics: [],
        },
      },
    },
  }

  const component = TestRenderer.create(
    <MockedProvider mocks={[dogMock]} addTypename={false}>
      <App />
    </MockedProvider>,
  )

  act(async () => {
    await new Promise((r) => setTimeout(r, 0))
  })

  const div = component.root.findByType('div')
  expect(div.children[0]).toContain('Name')
})
