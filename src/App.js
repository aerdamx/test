import './App.css'
import 'normalize.css'

import { useState } from 'react'
import { useQuery, gql } from '@apollo/client'
// get our fontawesome imports
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Components
import TopicList from './components/TopicList'
import Animation from './components/Animations'

// Utils
import { topicQueryMaker } from './utils/querys'

// Styled
import { OL, H1, DIV, SPAN } from './styled/index'

const App = () => {
  const [level, setLevel] = useState(1)
  // eslint-disable-next-line
  const [topic, setTopic] = useState('react')

  // Make the query
  const TOPIC_QUERY = gql`
    ${topicQueryMaker(level, topic)}
  `

  // Set the level
  const handleSetLevel = (number) => {
    setLevel(number)
  }

  const { loading, error, data } = useQuery(TOPIC_QUERY)

  if (loading) return <Animation type="loader" msg="Loading...." />

  if (error) return <Animation type="sad" msg="UPS...." />

  const {
    topic: { name, relatedTopics, stargazerCount },
  } = data
  return (
    <>
      <H1>
        {name}
        <SPAN size={0.7}>
          {'  '}
          {stargazerCount}
          {'  '}
        </SPAN>
        <SPAN size={0.7}>
          <FontAwesomeIcon icon={faStar} />
        </SPAN>
      </H1>
      <DIV>
        <OL>
          {relatedTopics &&
            relatedTopics.map((topic) => (
              <TopicList
                key={topic.id}
                handleSetLevel={handleSetLevel}
                {...topic}
                level={0}
              />
            ))}
        </OL>
      </DIV>
    </>
  )
}

export default App
