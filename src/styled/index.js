import styled from 'styled-components'

export const OL = styled.ol`
  display: inline-flex;
  flex-direction: column;
  text-align: left;
  list-style: none;
  position: relative;
  color: lightgoldenrodyellow;

  /* Set the counter. */
  counter-set: ${({ number }) => `my-awesome-counter ${number} `};
  /* The resulting marging get a bigger. */
  margin: ${({ number }) => `10px 0 10px ${number / 2}rem `};
  /* The resulting color get a random one. */
  background-color: ${({ number }) =>
    `rgb(${number * 2}, ${number * 2}, ${number * 2}, 0.5)`};

  &:before {
    content: counter(my-awesome-counter) '. ';
    color: lightgray;
    font-weight: bold;
    position: absolute;
    left: 0;
    margin-right: 10px;
    margin-bottom: 10px;
    width: 35px;
    height: 35px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    font-size: 16px;
    background-color: darkorange;
    border-radius: 50%;
    color: #fff;
  }
`

export const H1 = styled.h1`
  display: block;
  width: 100%;
  background-color: dodgerblue;
  font-size: 8em;
  text-align: center;
  margin: unset;
`
export const SPAN = styled.span`
  font-size: ${({ size }) => `${size}em`};
  color: ${({ color }) => (color ? color : 'black')};
`

export const DIV = styled.div`
  display: inline-flex;
  flex-direction: column;
`

export const LI = styled.li`
  display: block;
  text-align: left;
  counter-increment: my-awesome-counter;
  color: ${({ number }) => (number === 1 ? 'black' : 'lightpink')};
  padding-left: ${({ number }) => `${number / 2}rem`};
  padding: ${({ number }) => `10px ${number / 2}rem`};
  &:before {
    content: counter(my-awesome-counter) '. ';
    margin-right: 10px;
    margin-bottom: 10px;
    width: 35px;
    height: 35px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    font-size: 16px;
    background-color: #d7385e;
    border-radius: 50%;
    color: #fff;
  }
`
